package com.sample.junit.basepackage;

public class FunctionalClass {
	//private BaseClass base = new BaseClass();
	
	public Integer add(Integer input1, Integer input2){
		return input1 + input2;
	}
	
	public Integer sub(Integer input1, Integer input2){
		return input1 - input2;
	}
	
	public Integer multi(Integer input1, Integer input2){
		return input1 * input2;
	}
	
	public Integer divide(Integer input1, Integer input2){
		return input1 / input2;
	}
}
