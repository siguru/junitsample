package com.sample.junit.testpackage;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.sample.junit.basepackage.InputClass;
import com.sample.junit.TestCaseValues;
import com.sample.junit.TestFunction;
import com.sample.junit.basepackage.FunctionalClass;

// Allows to run multiple test cases for a single TEST-CLASS
@RunWith(Parameterized.class)
public class FunctionalClassTest {

	Class<?> className = FunctionalClass.class;
	Method method;
	String methodName;
	InputClass base;
	FunctionalClass function;
	TestCaseValues testcase;
	Map<String, TestCaseValues> functionMap = new HashMap<String, TestCaseValues>();


	public FunctionalClassTest(Map<String, TestCaseValues> functionMap) {
		System.out.println("CONSTRUCTOR");
		this.functionMap = (Map<String, TestCaseValues>)functionMap;
	}

	@Parameterized.Parameters
	public static Collection<?> testCases() {
		return new TestFunction().fileToObject("D:\\Work Files\\Workspace\\JUnit\\JUnitSample\\input-file.txt");
	}

	@BeforeClass
	public static void setUpClass(){
		System.out.println("SET UP");
	}

	@Test
	public void testAdd() {
		methodName = "add";
		testcase = functionMap.get(methodName);
		try {
			if(testcase == null){
				System.out.println("No Testcase Available");
				return ;
			}
			method = className.getDeclaredMethod(methodName, testcase.getParamTypes());
			assertEquals(30, method.invoke(className.newInstance(), testcase.getParameters()));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//fail("Not yet implemented");
		catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testSub() {
		methodName = "sub";
		testcase = functionMap.get(methodName);
		try {
			if(testcase == null){
				System.out.println("No Testcase Available");
				return ;
			}
			method = className.getDeclaredMethod(methodName, testcase.getParamTypes());
			assertEquals(30, method.invoke(className.newInstance(), testcase.getParameters()));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//fail("Not yet implemented");
		catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testMulti() {
		methodName = "multi";
		testcase = functionMap.get(methodName);
		try {
			if(testcase == null){
				System.out.println("No Testcase Available");
				return ;
			}
			method = className.getDeclaredMethod(methodName, testcase.getParamTypes());
			assertEquals(30, method.invoke(className.newInstance(), testcase.getParameters()));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//fail("Not yet implemented");
		catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testDivide() {
		methodName = "divide";
		testcase = functionMap.get(methodName);
		try {
			if(testcase == null){
				System.out.println("No Testcase Available");
				return ;
			}
			method = className.getDeclaredMethod(methodName, testcase.getParamTypes());
			assertEquals(30, method.invoke(className.newInstance(), testcase.getParameters()));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//fail("Not yet implemented");
		catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void setDownClass(){
		System.out.println("SET DOWN");
	}

}
