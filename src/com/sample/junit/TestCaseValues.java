package com.sample.junit;

public class TestCaseValues {

	// Can be added if needed
/*	private Class<?> className;
	private Method method;*/
	private Object[] parameters;
	private Class<?>[] paramTypes;
	private Object expectedOutput;
	private Class<?> outputType;
	
/*	public Class<?> getClassName() {
		return className;
	}
	public void setClassName(Class<?> className) {
		this.className = className;
	}
	public Method getMethod() {
		return method;
	}
	public void setMethod(Method method) {
		this.method = method;
	}*/
	public Object[] getParameters() {
		return parameters;
	}
	public void setParameters(Object[] parameters) {
		this.parameters = parameters;
	}
	public Class<?>[] getParamTypes() {
		return paramTypes;
	}
	public void setParamTypes(Class<?>[] paramTypes) {
		this.paramTypes = paramTypes;
	}
	public Object getExpectedOutput() {
		return expectedOutput;
	}
	public void setExpectedOutput(Object expectedOutput) {
		this.expectedOutput = expectedOutput;
	}
	public Class<?> getOutputType() {
		return outputType;
	}
	public void setOutputType(Class<?> outputType) {
		this.outputType = outputType;
	}
	
}
