package com.sample.junit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TestFunction {

	private Class<?> type;
	private Object param;

	// This function returns a map contains, "method-name:test-case-values"
	public List<Map<String, TestCaseValues>> fileToObject(String fileName){
		// TODO - Create i/o stream to dig-in dig-out
		File file = new File(fileName);
		Map<String,List<TestCaseValues>> testMap = new HashMap<String,List<TestCaseValues>>();
		List<TestCaseValues> testCases = new ArrayList<TestCaseValues>();
		BufferedReader reader;
		TestCaseValues testCase = new TestCaseValues();
		try {
			reader = new BufferedReader(new FileReader(file));
			String line = "";
			String[] dummyParams, dummyTypes;
			// Start of bean variables
			//			Class<?> className;
			//			Method method;
			Object[] parameters;
			Class<?>[] paramTypes;
			Object expectedOutput;
			Class<?> outputType;
			// End of bean variables
			while((line = reader.readLine()) != null){
				testCase = new TestCaseValues();
				String[] actualInput      = line.split(";");
				try {
					//					className             = Class.forName(actualInput[0]);
					//					testCase.setClassName(className);
					// Dummy - Object for storage
					dummyParams           = actualInput[2].split(",");
					dummyTypes            = actualInput[3].split(",");
					int paramLength       = dummyParams.length;
					paramTypes        = new Class[paramLength];
					parameters        = new Object[paramLength];
					//					if(paramLength<=0){
					//						method            = className.getDeclaredMethod(actualInput[1], null);
					//					} else {
					for(int i = 0; i < paramLength; i++){
						setParamAndTypes(dummyParams[i], dummyTypes[i]);
						parameters[i] = param;
						paramTypes[i] = type;
					}
					//						method            = className.getDeclaredMethod(actualInput[1], paramTypes); 
					//					}
					//					testCase.setMethod(method);
					testCase.setParameters(parameters);
					testCase.setParamTypes(paramTypes);
					setParamAndTypes(actualInput[4], actualInput[5]);
					// Can be stored to a bean
					expectedOutput        = param;
					outputType            = type;
					testCase.setExpectedOutput(expectedOutput);
					testCase.setOutputType(outputType);
				}catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(testMap.containsKey(actualInput[1])){
					testCases             = testMap.get(actualInput[1]);
				} else {
					testCases          = new ArrayList<TestCaseValues>();
				}
				testCases.add(testCase);
				testMap.put(actualInput[1], testCases);
			}
		}  catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Set<String> keySet = testMap.keySet();
		Iterator<String> iterator = keySet.iterator();
		Map<String, TestCaseValues> functionMap = new HashMap<String, TestCaseValues>();
		List<Map<String, TestCaseValues>> functionMapList = new LinkedList<>();
		String key;

		for(int i =0;i<10;i++){
			functionMap = new HashMap<String, TestCaseValues>();
			iterator = keySet.iterator();
			while(iterator.hasNext()){
				key = iterator.next();
				if(testMap.get(key).size() > i){
					testCase = testMap.get(key).get(i);
				} else {
					testCase = null;
				}
				functionMap.put(key, testCase);
			}
			functionMapList.add(functionMap);
		}
		//return functionMapList.toArray();
		return functionMapList;
	}

	public void setParamAndTypes(Object parameter, Object parameterType){
		switch(parameterType.toString().toLowerCase()){
		case "str":
			type  = String.class;
			param = parameter.toString();
			break;
		case "int":
			type  = Integer.class;
			param = new Integer(parameter.toString());
			break;
		case "float":
			type  = Float.class;
			param = new Float(parameter.toString());
			break;
		case "double":
			type  = Double.class;
			param = new Double(parameter.toString());
			break;
		}
	}

}
