package com.sample.junit;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import com.sample.junit.testpackage.FunctionalClassTest;

public class MainClass {

	public static void main(String[] args) {
/*		TestSuite testSuite = new TestSuite(FunctionalClassTest.class);
		TestResult testResult = new TestResult();
		testSuite.run(testResult)*/;

		Result result = JUnitCore.runClasses(FunctionalClassTest.class);
		for(Failure failure : result.getFailures()){
			System.out.println(failure);
			System.out.println(failure.getMessage());
			System.out.println(failure.getTestHeader());
		}
		System.out.println("Success !?"+result.wasSuccessful());
	}

}
